Visual Culture revisited to be faster, easier to install by using Gitlab API.

# To install
Move into the generator directory:
`cd generator`

Optional, make a virtual environment:
`virtualenv -p python3 venv`
`source venv/bin/activate`

To install the requirements
`pip install -r requirements.txt`

# To generate
`python generate.py`

# To serve:
`cd output`
`python3 -m http.server`