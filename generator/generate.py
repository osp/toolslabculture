import api
from jinja2 import Environment, FileSystemLoader, select_autoescape
import datetime, time
from models import Group
from settings import GROUP_ID, TEMPLATE_DIR, OUTPUT_DIR, CACHE_DIR, STATIC_DIR
import os.path
from os import makedirs, mkdir
import shutil


env = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR),
    autoescape=select_autoescape(['html', 'xml'])
)

STRPTIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
def format_datetime(value, format="%d/%m/%Y, %H:%M"):
    if (type(value) == str):
        value = datetime.datetime.strptime(value[:-10], STRPTIME_FORMAT)
    return value.strftime(format)

env.filters['datetime'] = format_datetime


print('Starting generation')

if not os.path.exists(OUTPUT_DIR):
  mkdir(OUTPUT_DIR)

if not os.path.exists(CACHE_DIR):
  mkdir(CACHE_DIR)

group = Group(GROUP_ID)
# For now invalidate group cache on every call
#group.invalidate_cache()
group.get()
# print(group)
# for project in group.projects:
  # print(hasattr(project, 'tree'))
  # print(project.links)
#   print(project.name)
#   print(project.tree)
# print(group.projects.models)

# def get_projects ():
#   call = api.group_projects(group_id = GROUP_ID)
#   return call.get()


# def get_tree (project_id):
#   call = api.tree(project_id)
#   return call.get()


# def get_commits (project_id):
#   call = api.commits(project_id)
#   return call.get()


# def make_local_url (url):
#   return url


# def parse_project (project):
#   return Project(id=project['id'], name=project['name'], tree = get_tree(project['id']), commits = get_commits(project['id']))

# print('Loading templates')
# projects = [parse_project(project) for project in get_projects()]

template = env.get_template('projects.html')
template.stream(projects=group.projects).dump(os.path.join(OUTPUT_DIR, 'index.html'))

print('Generating individual project pages')
project_template = env.get_template('project.html')
for project in group.projects:
  project_url = project.links['self']
  project_name = os.path.basename(project_url)
  project_path = os.path.dirname(project_url)
  output_path = os.path.join(OUTPUT_DIR, project_path)

  if not os.path.exists(output_path):
    makedirs(output_path)

  project_template.stream(project=project).dump(os.path.join(output_path, project_name))

print('Copying static files')
if os.path.exists(os.path.join(OUTPUT_DIR, 'static')):
  shutil.rmtree(os.path.join(OUTPUT_DIR, 'static'))
shutil.copytree(STATIC_DIR, os.path.join(OUTPUT_DIR, 'static'))

print('Generation finished')

# def update_project(project_id):
#   call = get_commits(project_id)
#   call.invalidate_cache()
#   call.get()
