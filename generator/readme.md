// deal with api error
// count attempts ? perhaps record timestamp
// 404 → cache

{
  'error': 404,
  'timestamp': x
}



tree: [  {
    "id": "591330eb757b8f3da46258f89b530bdac7c92e17",
    "name": "contour.html",
    "type": "blob",
    "path": "contour.html",
    "mode": "100644"
  }
]

commits: [  {
    "id": "e14c0a584be4dc086fc48938b848a16003cd22ea",
    "short_id": "e14c0a58",
    "created_at": "2019-05-03T12:48:33.000Z",
    "parent_ids": [],
    "title": "A new project: the remake of OSP website!",
    "message": "A new project: the remake of OSP website!\n",
    "author_name": "Stephanie Vilayphiou",
    "author_email": "stephanie@stdin.fr",
    "authored_date": "2019-05-03T12:48:33.000Z",
    "committer_name": "Stephanie Vilayphiou",
    "committer_email": "stephanie@stdin.fr",
    "committed_date": "2019-05-03T12:48:33.000Z"
  }
]

projects: [{
  "id": 510,
  "description": "",
  "name": "tools.gitlabculture",
  "name_with_namespace": "osp / tools.gitlabculture",
  "path": "tools.gitlabculture",
  "path_with_namespace": "osp/tools.gitlabculture",
  "created_at": "2019-05-03T12:49:46.699Z",
  "default_branch": "master",
  "tag_list": [],
  "ssh_url_to_repo": "git@gitlab.constantvzw.org:osp/tools.gitlabculture.git",
  "http_url_to_repo": "https://gitlab.constantvzw.org/osp/tools.gitlabculture.git",
  "web_url": "https://gitlab.constantvzw.org/osp/tools.gitlabculture",
  "readme_url": "https://gitlab.constantvzw.org/osp/tools.gitlabculture/blob/master/README.md",
  "avatar_url": null,
  "star_count": 0,
  "forks_count": 0,
  "last_activity_at": "2019-05-03T12:49:46.699Z",
  "namespace": {
    "id": 8,
    "name": "osp",
    "path": "osp",
    "kind": "group",
    "full_path": "osp",
    "parent_id": null
  },
  "_links": {
    "self": "https://gitlab.constantvzw.org/api/v4/projects/510",
    "issues": "https://gitlab.constantvzw.org/api/v4/projects/510/issues",
    "merge_requests": "https://gitlab.constantvzw.org/api/v4/projects/510/merge_requests",
    "repo_branches": "https://gitlab.constantvzw.org/api/v4/projects/510/repository/branches",
    "labels": "https://gitlab.constantvzw.org/api/v4/projects/510/labels",
    "events": "https://gitlab.constantvzw.org/api/v4/projects/510/events",
    "members": "https://gitlab.constantvzw.org/api/v4/projects/510/members"
  },
  "archived": false,
  "visibility": "public",
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": true,
  "issues_enabled": true,
  "merge_requests_enabled": true,
  "wiki_enabled": true,
  "jobs_enabled": true,
  "snippets_enabled": true,
  "shared_runners_enabled": true,
  "lfs_enabled": true,
  "creator_id": 25,
  "import_status": "none",
  "open_issues_count": 0,
  "public_jobs": true,
  "ci_config_path": null,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "request_access_enabled": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "printing_merge_request_link_enabled": true,
  "merge_method": "merge",
  "external_authorization_classification_label": null
}]