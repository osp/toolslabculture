import os.path

BASE_DIR = os.path.dirname(__file__)
CACHE_DIR = os.path.join(BASE_DIR, 'cache')
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
OUTPUT_DIR = os.path.join(BASE_DIR, 'output')
STATIC_DIR = os.path.join(BASE_DIR, 'static')
API_URL = "https://gitlab.constantvzw.org/api/v4/"
GROUP_ID = 8

DEBUG = True
MAX_PAGES = 999
PER_PAGE = 100