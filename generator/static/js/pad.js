var head  = document.getElementsByTagName('head')[0];

// GETS CSS FROM FRAMAPAD
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    let css = document.createElement('style');
    css.innerHTML = this.responseText;
    head.appendChild(css);
  }
};
xhttp.open("GET", "https://semestriel.framapad.org/p/9f7m-osp-website-css/export/txt", true);
xhttp.send();
