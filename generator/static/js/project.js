// TEST VUE COMPONENTS
//var filterTree = new Vue({
//  el: '#filter-tree',
//  data: {
//    name: 'Vue.js'
//  },
//  // define methods under the `methods` object
//  methods: {
//    greet: function (event) {
//      // `this` inside methods points to the Vue instance
//      alert('Hello ' + this.name + '!')
//      // `event` is the native DOM event
//      if (event) {
//        alert(event.target.tagName)
//      }
//    }
//  }
//})







// T R E E
const hiddenFile = /^\./;
var fileList = [];
var formerTree = [];
var blobHistory = [];
const project_path = [gitlabURL, user_name, projectName].join('/');

// GET ROOT TREE
var treeURL = gitlabApiURL + repoID + "/repository/tree?per_page=100";
fetch(treeURL).then(function(resp){ return resp.json(); })
.then(function(data){
    return data.map(function(file) {
		if (! file["name"].match(hiddenFile)) {
			fileList.push(file);
		 }
    })
})
 
function browseTree(path){
	var treeURL = gitlabApiURL + repoID + "/repository/tree?per_page=100&path="+ path;
	vm.fileList.splice(0);
	fetch(treeURL).then(function(resp){ return resp.json(); })
	.then(function(data){
		return data.map(function(file) {
			if (! file["name"].match(hiddenFile)) {
				vm.fileList.push(file);
			}
		})
	})
}

var treeApp = new Vue({
    el: '#tree',
    data: {
      fileList,
	  upPath: "/",
	  blobItem: "",
	  blobHistory
    },
    delimiters: ['[[',']]'],
	methods: {
		tree: function(type, path, blobID){
			vm = this;

			if (type == "tree") {
				currentPath = path;
                browseTree(path);

			} else if (type == "up") {
				upPath = currentPath.split("/");
				upPath.pop();
				currentPath = upPath.join("");

                browseTree(upPath);

			} else if (type == "blob") {
				let blobURL = gitlabApiURL + repoID + "/repository/blobs/" + blobID + "/raw";
				fetch(blobURL).then((resp) => resp.blob())
				.then(function(data){
					var mime = data["type"];

					// FILENAME
					h4 = "<h4>" + path + "</h4>"

        			// DOWNLOAD RAW FILE
        			href = project_path + "/raw/master/" + path;
        			a = '<p><a class="download-me" download href="' + href + '">Download file</a></p>';


                    // DIFF: SHOWS ALL VERSIONS OF ONE PICTURE ////////////////////////////////////
                    vm.blobHistory.splice(0);
                    const file_commits_url = gitlabApiURL  + repoID + "/repository/commits?path=" + path;
                    fetch(file_commits_url)
                    .then((resp) => resp.json())
                    .then(function(data){
                        return data.map(function(file) {
                            let f = {};
                            blob_id = file.id;

                            date = new Date(file.created_at);
							f.date = date.toLocaleString();
                            f.msg = file.message;
                            f.author = file.author_name;
                            f.href = project_path + "/-/raw/" + blob_id + "/" + path ;

                            path = path.replace(/\//g, "%2F");

                            if(mime == "image/png" | mime == "image/jpeg" | mime == "image/jpg" | mime == "image/gif") {
                                const commit_url = gitlabApiURL  + repoID + "/repository/files/" + path + "?ref=" + blob_id;
                                fetch(commit_url)
                                .then((resp) => resp.json())
                                .then(function(data){
                                    f.b64 = 'data:image/jpeg;base64,' + data['content'];
                                    blobHistory.push(f);
                                });
                            } else {
								blobHistory.push(f);
                            }
                        });
                    });

					// if PNG, JPG, or GIF
					if(mime == "image/png" | mime == "image/jpeg" | mime == "image/jpg" | mime == "image/gif") {
						vm.blobItem = h4 + a + "<img src='" + blobURL+ "'i>";
					} 
					// if TXT, SVG, code...
					else if (mime == "text/plain" | mime == "image/svg+xml") {
						var reader = new FileReader();
						reader.readAsText(data);
						reader.onloadend=function(){
							vm.blobItem = h4 + a + reader.result;
						};
					}
					else {
						vm.blobItem = h4 + a;
					}
				})
			}
		},
	}
})






// I M A G E S
var icebergList = [];
var processusList = [];
let icebergURL = gitlabApiURL  + repoID + "/repository/tree?path=iceberg";
let processusURL = gitlabApiURL  + repoID + "/repository/tree?path=processus";
getImages(repoID, icebergURL, icebergList);
getImages(repoID, processusURL, processusList);

var icebergApp = new Vue({
    el: '#iceberg',
    data: {
      icebergList
    }
})

var processusApp = new Vue({
    el: '#processus',
    data: {
      processusList
    }
})

function getImages(repoID, apiURL, imageList){
    fetch(apiURL).then(function(resp){ return resp.json(); })
    .then(function(data){
        return data.map(function(image) {
            item = {};
            item.name = image.name;
            item.src = project_path + "/raw/" + projectBranch + "/" + image.path;
            imageList.push(item);
        })
    });
}
